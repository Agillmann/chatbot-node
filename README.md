# Chatbot Express

## Description

Chatbot en Node.js et MongoDB

[Demo](https://agillmann-chatbot-node.herokuapp.com/)

### Prérequis

Pour pouvoir installer le projet sur votre machine il vous faut :

* Node.js

* npm

* docker

### Installation

``` bash
git clone https://gitlab.com/Agillmann/chatbot-node
```  

``` bash
npm install
```  

``` bash
npm start
```

### MongoDB

``` bash
docker run -d -p 27017:27017 --name mongoDB mongo:4
```  

``` bash
docker run -it --link mongoDB:mongo --rm mongo:4 mongo --host mongo test
```  

## Auteurs

**Adrien Gillmann** - *Maintainer* - [Gitlab](https://gitlab.com/Agillmann)
