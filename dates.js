const MongoClient = require('mongodb').MongoClient;

(async () => {
  // Connection URL
  const mongo_uri = 'mongodb://localhost:27017/test';
  // Database Name
  const dbName = 'test';
  const client = new MongoClient(mongo_uri, {
    useNewUrlParser: true
  });
  try {
    await client.connect();
    console.log('Connected correclty to server');

    const db = client.db(dbName);

    // Get the collection
    const col = db.collection('dates');

    // Insert multiple documents
    const r = await col.insertMany([{
      date: new Date()
    }]);

    // Get first two documents that match the query
    const docs = await col.find({}).toArray();
    console.log({
      docs
    });

  } catch (err) {
    console.log(err.stack);
  }
})();