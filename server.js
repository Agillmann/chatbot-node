const express = require("express");
const fs = require("fs");
const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
const PORT = process.env.PORT || 3000;
const MONGODB_URL = process.env.MONGOLAB_WHITE_URI  || 'mongodb://localhost:27017/chat-bot';
console.log(process.env.MONGOLAB_WHITE_URI);
console.log('PLZZ !!!');
const dbName = process.env.DBNAME || 'chat-bot';
const app = express();
// "mongodb://heroku_f46tbw9p:heroku_f46tbw9p@ds141815.mlab.com:41815/heroku_f46tbw9p"
app.use(express.json(), express.urlencoded({
  extended: true
}));

/////////////////////////////// MongoDB connect

const client = new MongoClient(MONGODB_URL, {
  useNewUrlParser: true
});
let col;

(async () => {
  try {
    await client.connect();
    console.log('Connected correclty to MongoDB database');
    const db = client.db(dbName);

    // Get the collection
    col = db.collection('messages');

  } catch (err) {
    console.log(err.stack);
  }
  // client.close()
})();

/////////////////////////////// Functions 

// Check message from json file, read and write file.json
const checkInfo = (msg, jsonFile) => {
  let contentFile = JSON.parse(fs.readFileSync(jsonFile, 'utf8'));
  for (let i = 0; i < msg.length; i++) {
    if (msg[i] === '=') {
      let str = msg.split('=');
      contentFile[str[0].trim()] = str[1].trim();
      fs.writeFileSync(jsonFile, JSON.stringify(contentFile), 'utf8');
      return ('Merci pour cette information !');
    }
  }
  for (let key in contentFile) {
    if (contentFile.hasOwnProperty(msg)) {
      return (msg + ': ' + contentFile[msg]);
    } else {
      return ('Je ne connais pas ' + msg + '...');
    }
  }
}

/////////////////////////////// ROUTE

// GET home page
app.get('/', (_req, res) => {
  res.sendFile(__dirname + '/index.html');
})

// GET hello
app.get('/hello', (_req, res) => {
  res.send('Hello World!');
})

// POST chat
app.post('/chat', (req, res) => {
  let botResponse = checkInfo(req.body.msg.toLowerCase(), 'réponses.json');
  res.send(botResponse);
})

// POST chat-bot
app.post('/chat-bot', async (req, res) => {
  let userMsg = req.body.msg.toLowerCase();
  let botResponse = checkInfo(userMsg, 'réponses.json');
  // Insert to MongoDB 
  await col.insertMany([{
    from: 'user',
    msg: userMsg
  }, {
    from: 'bot',
    msg: botResponse
  }], (err, result) => {
    test.equal(null, err);
    console.log('Insertion de l\'échange dans l\'historique', result);
  });
  res.send(botResponse);
});

// GET messages/all
app.get('/messages/all', async (_req, res) => {
  // Get all messages
  const docs = await col.find({}).toArray();
  console.log({
    docs
  });
  res.send(docs);
});

// Delete messages/last
app.delete('/messages/last', async (_req, res) => {
  // Get 2 last messages
  const lastMessages = await col.find().sort({
    _id: -1
  }).limit(2).toArray();
  if (lastMessages.length) {
    let ids = [lastMessages[0]._id, lastMessages[1]._id];
    // Delete lastMessages 
    await col.deleteMany({
      _id: {
        $in: ids
      }
    });
    console.log('Le dernier échange de l’historique a été supprimé');
    res.send('Le dernier échange de l’historique a été supprimé');
  } else {
    console.log('Rien a supprimer');
    res.send('Il n\'y a pas de messages dans l\'historique');
  }
});

app.listen(PORT, () => {
  console.log('Serveur en route port', PORT);
});
